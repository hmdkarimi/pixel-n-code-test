import Colors from '../vars/variable';
import CommonStyles from '../styles/Common';
export default {
  navbarProps: {
    navigationBarStyle: { backgroundColor: 'trasnparent' },
    titleStyle: {
      color: Colors.brandText,
      alignSelf: 'center',
      letterSpacing: 2,
      fontSize: Colors.fontSizeBase,
    },
    backButtonTintColor: Colors.textColor,
  },

  tabProps: {
    swipeEnabled: true,
    activeBackgroundColor: 'rgba(255,255,255,0.1)',
    inactiveBackgroundColor: Colors.brandWhite,
    tabBarStyle:  [CommonStyles.Bottombar],
  },

  icons: {
    style: { color: 'white', height: 30, width: 30 },
  },
};
