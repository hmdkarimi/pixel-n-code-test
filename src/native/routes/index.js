import React from 'react';
import { Scene, Stack , Router , Lightbox ,Modal } from 'react-native-router-flux';
import {Image,View} from 'react-native';
import DefaultProps from '../constants/navigation';
import BackBtn from '../../native/components/UI/BackBtn';
import { Actions } from 'react-native-router-flux';
/*****INTRO SCREEN CONTAINER & VIEW COMPONENT */
import IntroContainer from '../../containers/Intro';
import IntroComponent from '../components/Intro/Intro';


/*****WELCOME SCREEN CONTAINER & VIEW COMPONENT */
import WelcomeContainer from '../../containers/Welcome';
import WelcomeComponent from '../components/Welcome/Welcome';

/*****FAME LIST SCREEN CONTAINER & VIEW COMPONENT */
import FameListContainer from '../../containers/FameList';
import FameListComponent from '../components/FameList/FameList';

const Index = (

<Router hideNavBar>
  <Stack>
	<Scene
	  key="intro"
	  {...DefaultProps.navbarProps}
	  component={IntroContainer}
	  Layout={IntroComponent}
	  initial
	/>

	<Scene
	  key="fameList"
	  {...DefaultProps.navbarProps}
	  component={FameListContainer}
	  Layout={FameListComponent}
	/>

	<Scene
	  key="welcome"
	  {...DefaultProps.navbarProps}
	  component={WelcomeContainer}
	  Layout={WelcomeComponent}
	  back
	  renderBackButton={() =>
		<BackBtn
			action={() => Actions.fameList()}
		/>
   	  }

	/>

	<Scene
	  key="fameList"
	  {...DefaultProps.navbarProps}
	  component={FameListContainer}
	  Layout={FameListComponent}
	  back
	  renderBackButton={() =>
		<BackBtn
			action={() => Actions.welcome()}
		/>
   	  }
	/>
  </Stack>
</Router>

);

export default Index;
