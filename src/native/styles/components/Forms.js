import { StyleSheet, Dimensions, SCREEN_HEIGHT, Platform } from 'react-native';
import options from '../../vars/variable';
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;
const { height, width } = Dimensions.get('window');

const formsStyles = StyleSheet.create({

  ///Common Styles for TEXT FORM
  input: {
	fontSize: options.baseSize,
	color: options.brandText,
	fontFamily: options.fontFamily,
	borderRadius: options.borderRadiusBase,
	backgroundColor: '#F1F2F6',
	paddingVertical: options.baseOffset,
	paddingHorizontal: options.mediumOffset,
	marginVertical:5,

  },
  digitInput:{
	backgroundColor: '#F1F2F6',
	paddingVertical: options.baseOffset,
	margin:0,
	paddingHorizontal: options.baseOffset,
	borderRadius: options.borderRadiusBase,
	fontSize: 30,
	color: options.brandText,
	fontFamily: options.fontFamilyBold,
	width:60,
	height:60
  },

});
export default formsStyles;
