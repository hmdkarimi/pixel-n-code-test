import { StyleSheet, Dimensions, SCREEN_HEIGHT, Platform } from 'react-native';
import options from '../../vars/variable';
import utilityStyles from '../Utitlity';
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;
const { height, width } = Dimensions.get('window');

const listStyles = StyleSheet.create({
  ///Styles for UserList
  listContent:{
	borderBottomColor: '#F1F2F4',
	borderBottomWidth:1,
	paddingVertical:15,
	paddingRight: options.mediumOffset
  },
  statusBullet:{
	width:14,
	height:14,
	borderWidth:2,
	borderColor:'#fff',
	backgroundColor:options.brandSuccess,
	borderRadius:7,
	position:'absolute',
	right:6,
	bottom:-2
  },
  statusText:{
	fontSize:11,
	color:options.brandTitle,
	fontFamily:options.fontFamilySemibold,
	textAlign:'center',
	marginTop:10,
	alignSelf:'center',
	flexDirection:'row',
  },
  badge:{
	width:20,
	height:20,
	backgroundColor:options.brandAlert,
	borderRadius:10,
	borderWidth:1,
	borderColor:'#fff',
	position:'absolute',
	right:4,
	bottom:-3,
	flexDirection:'row',
	alignItems:'center',
	zIndex:2,
	elevation:2,
	justifyContent:'center'


  },

  badgeTxt: {

  	color: '#fff',
  	textAlign: 'center',
  	fontSize: 12,
  	fontFamily: options.fontFamily,



  },

  iconList: {
	borderBottomColor: '#F1F2F4',
	borderBottomWidth:1,
  },
  iconListIcon:{
	  width:30,
	  height:30,
	  flexDirection:'row',
	  alignItems:'center',
	  justifyContent:'center',
	  borderRadius:30,
	  textAlign:'center'
  },
  storyModeAvatar:{
	  width:49,
	  height:49,
	  borderStyle:'dotted',
	  borderWidth:1,
	  borderColor:options.brandPrimary,
	  borderRadius:25
  },
  avatarBadge:{
	paddingHorizontal:1,
	paddingVertical:3,
	borderRadius:30,
	backgroundColor:options.brandPrimary,
	width:'100%',
	position:'absolute',
	flexWrap:'nowrap',
	left:0,
	bottom:-4
  },

  avatarBadgeText: {
  	textTransform: 'uppercase',
  	fontSize: 7,
  	fontFamily: options.fontFamilyBold,
  	color: "#fff",
  	textAlign: 'center',
  	width: '100%',
  },


});
export default listStyles;
