import { StyleSheet, Dimensions, SCREEN_HEIGHT, Platform } from 'react-native';
import options from '../vars/variable';
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;
const { height, width } = Dimensions.get('window');

const utilityStyle = StyleSheet.create({
  row: {
	display: 'flex',
	flexDirection: 'row',
	alignSelf: 'stretch'
  },
  column: {
	display: 'flex',

	flexDirection: 'column'
  },
  justifyBetween: {
	justifyContent: 'space-between'
  },
  justifyCenter: {
	justifyContent: 'center'
  },
  justifyStart: {
	justifyContent: 'flex-start'
  },
  justifyEnd: {
	justifyContent: 'flex-end'
  },
  alignCenter: {
	alignItems: 'center'
  },
  alignStart: {
	alignItems: 'flex-start'
  },
  alignEnd: {
	alignItems: 'flex-end'
  },
  alignSelfCenter: {
	alignSelf: 'center'
  },
  alignSelfEnd:{
	alignSelf: 'flex-end'
  },
  alignSelfStart:{
	alignSelf: 'flex-start'
  },
  displayFlex: {
	display: 'flex'
  },
  displayFlexFull: {
	flex: 1
  },
  fullWidth: {
	width: '100%'
  },
  fullHeight: {
	height: '100%'
  },
  flexGrow: {
	flexGrow: 10
  },
  relative:{
	position:'relative'
  },

  ///RADIUS / BORDERS
  radius: {
	borderRadius: options.borderRadiusBase
  },
  boxRadius: {
	borderRadius: options.borderRadiusBox
  },
  shadow: {
	elevation: 10,
	shadowColor: options.brandText,
	shadowOffset: { width: 0, height: 5 },
	shadowOpacity: 0.3,
	shadowRadius: 10
  },
  shadowBlue: {

	elevation: 10,
	shadowColor: '#0070FF',
	shadowOffset: { width: 0, height: 5 },
	shadowOpacity: 0.3,
	shadowRadius: 10
  },
  shadowPink: {
	elevation: 10,
	shadowColor: '#FF2D55',
	shadowOffset: { width: 0, height: 5 },
	shadowOpacity: 0.3,
	shadowRadius: 10
  },
  shadowBtn: {
	elevation: 10,
	shadowColor: options.brandPrimary,
	shadowOffset: {
	  width: 0,
	  height: 5
	},
	shadowOpacity: 0.3,
	shadowRadius: 10
  },
  noShadow:{
	  shadowOpacity: 0,
  },
  border: {
	borderWidth: 1,
	borderColor: options.brandText
  },

  ///FONT WEIGHT
  boldWeight: {
	fontFamily: options.fontFamilyBold
  },
  normalWeight: {
	fontFamily: options.fontFamily
  },

  semiboldWeight: {
	fontFamily: options.fontFamilySemibold
  },

  capitalize:{
	textTransform:'capitalize'
  },

  uppercase: {
	textTransform: 'uppercase'
  },

  ///FONT SIZE
  baseFontSize: {
	fontSize: options.baseSize
  },
  regularFontSize: {
	fontSize: options.regularSize
  },
  smallFontSize: {
	fontSize: options.smallSize
  },
  xsmallFontSize: {
	fontSize: 11
  },
  largeFontSize: {
	fontSize: options.largeSize
  },
  xlargeFontSize: {
	fontSize: options.xlargeSize
  },
  xlargeFontSizex: {
  	fontSize: options.xlargeSizex
  },
  xxlargeFontSize: {
	fontSize: options.xxlargeSize
  },
  xxxlargeFontSize: {
  	fontSize: 40
  },
  textAlignCenter: {
	textAlign: 'center'
  },
  textAlignLeft: {
	textAlign: 'left'
  },
  ///FONT COLOR
  grayColor: {
	color: options.brandText
  },
  primaryColor: {
	color: options.brandPrimary
  },
  darkColor: {
	color: options.brandTitle
  },
  blackColor:{
	color: '#000'
  },
  successColor: {
	color: options.brandSuccess
  },
  purpleColor: {
	color: options.brandLike
  },
  redColor: {
	color: options.brandAlert
  },
  blueColor: {
	color: options.brandBlue
  },
  lightColor: {
	color: options.brandWhite
  },

  hasBorderBottom:{
	borderBottomColor:'#E1E4E8',
	borderBottomWidth:1
  },

  ///BACKGROUND COLOR
  grayBkg: {
	backgroundColor: options.brandText
  },
  primaryBkg: {
	backgroundColor: options.brandPrimary
  },
  darkBkg: {
	backgroundColor: options.brandTitle
  },
  lightGrayBkg: {
	backgroundColor: options.brandText
  },
  lightBkg: {
	backgroundColor: options.brandWhite
  },
  darkGrayBkg: {
	  backgroundColor: "#F1F2F6"
  },
  xlightGrayBkg: {
	backgroundColor: '#F7F8FA'
  },



  //////SMALL OFFSETS
  smallMarginVertical: {
  		marginVertical: options.smallOffset
  	},
  	smallMarginHorizontal: {
  		marginVertical: options.smallOffset
  	},
  	smallMarginRight: {
  		marginRight: options.smallOffset
  	},
  	smallMarginBottom: {
  		marginBottom: options.smallOffset
  	},
  	smallMarginLeft: {
  		marginLeft: options.smallOffset
  	},
  	smallMarginTop: {
  		marginTop: options.smallOffset
  	},

  	smallPaddingVertical: {
  		paddingVertical: options.smallOffset
  	},
  	smallPaddingHorizontal: {
  		paddingHorizontal: options.smallOffset
  	},

  	smallPaddingRight: {
  		paddingRight: options.smallOffset
  	},
  	smallPaddingBottom: {
  		paddingBottom: options.smallOffset
  	},
  	smallPaddingLeft: {
  		paddingLeft: options.smallOffset
  	},
  	smallPaddingTop: {
  		paddingTop: options.smallOffset
  	},
  ////OFFSETS
  mediumMarginVertical: {
	  marginVertical: options.mediumOffset
	},
	mediumMarginHorizontal: {
	  marginVertical: options.mediumOffset
	},
	mediumMarginRight: {
	  marginRight: options.mediumOffset
	},
	mediumMarginBottom: {
	  marginBottom: options.mediumOffset
	},
	mediumMarginLeft: {
	  marginLeft: options.mediumOffset
	},
	mediumMarginTop: {
	  marginTop: options.mediumOffset
	},

	mediumPaddingVertical: {
	  paddingVertical: options.mediumOffset
	},
	mediumPaddingHorizontal: {
	  paddingHorizontal: options.mediumOffset
	},
	mediumPaddingRight: {
	  paddingRight: options.mediumOffset
	},
	mediumPaddingBottom: {
	  paddingBottom: options.mediumOffset
	},
	mediumPaddingLeft: {
	  paddingLeft: options.mediumOffset
	},
	mediumPaddingTop: {
	  paddingTop: options.mediumOffset
	},

	//////BASE OFFSETS
	baseMarginVertical: {
	  marginVertical: options.baseOffset
	},
	baseMarginHorizontal: {
	  marginVertical: options.baseOffset
	},
	baseMarginRight: {
	  marginRight: options.baseOffset
	},
	baseMarginBottom: {
	  marginBottom: options.baseOffset
	},
	baseMarginLeft: {
	  marginLeft: options.baseOffset
	},
	baseMarginTop: {
	  marginTop: options.baseOffset
	},

	basePaddingVertical: {
	  paddingVertical: options.baseOffset
	},
	basePaddingHorizontal: {
	  paddingHorizontal: options.baseOffset
	},

	basePaddingRight: {
	  paddingRight: options.baseOffset
	},
	basePaddingBottom: {
	  paddingBottom: options.baseOffset
	},
	basePaddingLeft: {
	  paddingLeft: options.baseOffset
	},
	basePaddingTop: {
	  paddingTop: options.baseOffset
	},


	//////XBASE OFFSETS
	xbaseMarginVertical: {
	  marginVertical: options.xbaseOffset
	},
	xbaseMarginHorizontal: {
	  marginVertical: options.xbaseOffset
	},
	xbaseMarginRight: {
	  marginRight: options.xbaseOffset
	},
	xbaseMarginBottom: {
	  marginBottom: options.xbaseOffset
	},
	xbaseMarginLeft: {
	  marginLeft: options.xbaseOffset
	},
	xbaseMarginTop: {
	  marginTop: options.xbaseOffset
	},

	xbasePaddingVertical: {
	  paddingVertical: options.xbaseOffset
	},
	xbasePaddingHorizontal: {
	  paddingHorizontal: options.xbaseOffset
	},

	xbasePaddingRight: {
	  paddingRight: options.xbaseOffset
	},
	xbasePaddingBottom: {
	  paddingBottom: options.xbaseOffset
	},
	xbasePaddingLeft: {
	  paddingLeft: options.xbaseOffset
	},
	xbasePaddingTop: {
	  paddingTop: options.xbaseOffset
	},

	//////SMALL OFFSETS
	largeMarginVertical: {
	  marginVertical: options.largeOffset
	},
	largeMarginHorizontal: {
	  marginVertical: options.largeOffset
	},
	largeMarginRight: {
	  marginRight: options.largeOffset
	},
	largeMarginBottom: {
	  marginBottom: options.largeOffset
	},
	largeMarginLeft: {
	  marginLeft: options.largeOffset
	},
	largeMarginTop: {
	  marginTop: options.largeOffset
	},

	largePaddingVertical: {
	  paddingVertical: options.largeOffset
	},
	largePaddingHorizontal: {
	  paddingHorizontal: options.largeOffset
	},
	largePaddingRight: {
	  paddingRight: options.largeOffset
	},
	largePaddingBottom: {
	  paddingBottom: options.largeOffset
	},
	largePaddingLeft: {
	  paddingLeft: options.largeOffset
	},
	largePaddingTop: {
	  paddingTop: options.largeOffset
	},




});
export default utilityStyle;
