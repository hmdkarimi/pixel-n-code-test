import React , {useState,useRef} from 'react';
import {View,Text,TextInput,ScrollView} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import {Actions} from 'react-native-router-flux';

///import styles & variables
import utilityStyles from "../../styles/Utitlity";
import formStyles from '../../styles/components/Forms';
import options from "../../vars/variable";

/// UI components
import Button from "../UI/Button";
import Spacer from "../UI/Spacer";
import Form from '../UI/Form';


const Welcome = (props) => {

  const {_renderItem,member} = props;
  //define hooks for collect and update data in component
  const [activeItem , setActiveItem] = useState(0);
  const _slider = useRef(null);

  //update slider active index
  const changeSlide = (index = 1) => {
	setActiveItem(index)
  }
  return (
	<ScrollView
		contentContainerStyle={[
			utilityStyles.column,
			utilityStyles.displayFlexFull,
			utilityStyles.lightBkg,
			utilityStyles.justifyCenter,
			utilityStyles.alignCenter,
			utilityStyles.fullHeight,
			utilityStyles.fullWidth,

		]}
	>
		<View style={[
			utilityStyles.mediumPaddingHorizontal,
			utilityStyles.lightBkg,
			utilityStyles.fullWidth,
		]}>
			<View>
				  <Carousel
				  ref={_slider}
				  inactiveSlideScale={1}
				  inactiveSlideOpacity={0}
				  autoplay={true}
				  autoplayInterval={5000}
				  loop={true}
				  autoplayDelay={0}
				  shouldOptimizeUpdates={false}
				  data={props.member.slides}
				  renderItem={_renderItem }
				  sliderWidth={options.deviceWidth}
				  itemWidth={options.deviceWidth}
				  contentContainerCustomStyle={[
					utilityStyles.alignEnd,
					utilityStyles.alignSelfCenter
				  ]}
				  containerCustomStyle={[utilityStyles.alignSelfCenter]}
				  onSnapToItem={(index) => changeSlide(index) }
				/>
				<Pagination
				  carouselRef={_slider}
				  dotsLength={props.member.slides.length}
				  activeDotIndex={activeItem}
				  dotColor={options.brandPrimary}
				  inactiveDotColor={options.brandText}
				  inactiveDotOpacity={1}
				  inactiveDotScale={1}

				/>
				</View>
				<Spacer size={30}/>
				<Form/>
		</View>
	</ScrollView>

	);
}



export default Welcome;

