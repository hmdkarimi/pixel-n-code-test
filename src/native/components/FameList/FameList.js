import React, { useState } from 'react';
import {View,FlatList} from 'react-native';

///import styles & variables

import utilityStyles from "../../styles/Utitlity";


/// custom components
import UserList from '../UI/UserList';



const FameList = (props) => {
	const {member} = props;
	return (
	  	<View style={[
		  	utilityStyles.column,
			utilityStyles.alignStart,
			utilityStyles.justifyStart,
			utilityStyles.displayFlexFull,
			utilityStyles.lightBkg,
			utilityStyles.mediumPaddingTop
		]}>
		  <View style={[
			  utilityStyles.lightBkg,
			  utilityStyles.fullWidth,
		  ]}>
			<FlatList
					data={member.list}
					contentContainerStyle={[utilityStyles.xbasePaddingVertical,{backgroundColor:'transparent'}]}
					numColumns={1}
					style={{backgroundColor:'transparent'}}
					keyExtractor={item =>  item.id}
					renderItem={({ item ,index}) => <UserList data={item} key={item.id}/> }
				/>
		  </View>
	  </View>
	);
}

FameList.propTypes = {

};

FameList.defaultProps = {

};

export default FameList;
