import React from 'react';
import PropTypes from 'prop-types';
import { View , TouchableOpacity ,Image ,Text} from 'react-native';

///import styles & variables
import utilityStyles from "../../styles/Utitlity";
import btnStyles from '../../styles/components/Buttons';
import userListStyles from '../../styles/components/List';
/// custom components
import Avatar from './Avatar';
import Spacer from './Spacer';

const UserList = (props) => {
	const {data} = props;
	return(
		<View style={[
			utilityStyles.row,
			utilityStyles.justifyBetween,
			utilityStyles.alignCenter,
			utilityStyles.relative,
			utilityStyles.baseMarginBottom

		]}>
			<View style={[utilityStyles.baseMarginRight,utilityStyles.mediumMarginLeft]}>
				<Avatar source={[data.image]} width={100} height={100} circle={false} />
			</View>
			<View style={[
			utilityStyles.row,
			utilityStyles.alignCenter,
			utilityStyles.flexGrow,
			utilityStyles.justifyBetween,
			userListStyles.listContent,

			]}>
			<View style={[utilityStyles.column]}>
				<Text style={[utilityStyles.capitalize,utilityStyles.semiboldWeight,utilityStyles.darkColor,utilityStyles.baseFontSize]}>
					{data.name}
				</Text>
				<Spacer size={3}/>
				<Text style={[utilityStyles.capitalize,utilityStyles.normalWeight,utilityStyles.grayColor,utilityStyles.smallFontSize]}>
					{data.dob}
				</Text>
			</View>
			</View>
		</View>
	);
}
export default UserList;
