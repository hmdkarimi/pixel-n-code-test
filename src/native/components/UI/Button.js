import React from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity ,Text ,View} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

///import styles & variables
import utilityStyles from "../../styles/Utitlity";
import btnStyles from '../../styles/components/Buttons';
import options from "../../vars/variable";


/*****BUTTON COMPOENET
 * Availabel config:
 * hasIcon => {
 * iconName: icon name (available icons: https: //ionicons.com/)
   iconSize: icon size
   iconColor: icon color
 * }
 */
const Button = (props) => {
	const {text,hasIcon,iconName,iconSize,iconColor,style,block} = props;
	return (
		<TouchableOpacity {...props} style={[

		  utilityStyles.shadowBtn,
		  utilityStyles.lightBkg,
		  utilityStyles.radius,
		  block ? utilityStyles.fullWidth : '',
		  style

		]}>
			<View
			  style={[
				btnStyles.btn ,
				utilityStyles.alignCenter,
				utilityStyles.row,
				utilityStyles.justifyCenter,

				]}
			>
			  {
				hasIcon
				? <Ionicons style={{marginRight:7}} name={iconName} size={iconSize} color={iconColor} />
				:null
			  }

			  <Text style={[
				utilityStyles.lightColor,
				utilityStyles.baseFontSize,
				utilityStyles.semiboldWeight,
				utilityStyles.textAlignCenter,

			  ]}>

				{text}

			  </Text>
			</View>
		</TouchableOpacity>
	);
}

Button.propTypes = {
  text: PropTypes.string,
  radius: PropTypes.bool,
  onPress: PropTypes.func,
  hasIcon: PropTypes.bool,
  block: PropTypes.bool,
};

Button.defaultProps = {
  text: 'Missing text',
  radius: false,
  onPress:null,
  hasIcon:false,
  block:false,
  iconSize:20,
  iconColor:'#FFF',
  iconName:null,
  action:null
};

export default Button;
