import React from 'react';
import { TouchableOpacity ,BackHandler} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Ionicons } from '@expo/vector-icons';


const BackBtn = ({ action }) => {
	let lastTap = null;
	const handleDoubleTap = () => {
		const now = Date.now();
		if (lastTap && (now - lastTap) < 500){
			alert('i cant use this package on expo to exit from app! ( https://www.npmjs.com/package/react-native-exit-app)')
			BackHandler.exitApp()
		} else {
			lastTap = now;
			action()
		}
	}

	return(
		<TouchableOpacity onPress={() => handleDoubleTap()}>
			<Ionicons style={{marginLeft:20}} size={24} color="#000" name="ios-arrow-back"></Ionicons>
		</TouchableOpacity>
	);
}


export default BackBtn;
