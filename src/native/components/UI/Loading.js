import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import Colors from '../../vars/variable';

const Loading = () => (
  <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
	<ActivityIndicator size="large" color={Colors.brandPrimary} />
  </View>
);

export default Loading;
