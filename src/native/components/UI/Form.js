import React , {useState,useRef} from 'react';
import {View,TextInput} from 'react-native';
import { connect } from 'react-redux';
///import styles & variables
import utilityStyles from "../../styles/Utitlity";
import formStyles from '../../styles/components/Forms';
import options from "../../vars/variable";

/// UI components
import Button from "../UI/Button";
import Spacer from "../UI/Spacer";



const Form = (props) => {
	const [inputValue, setInputValue] = useState(1);
	///update "inputValue" on change
	const changeInputValue = (val = 1) => {
		//update input value
		setInputValue(val)
	}
 	 //update slider based on new slides
  	const updateArray = (type='save') => {
		const {shuffleSlideArray,member} = props;
		shuffleSlideArray({type:type,value:inputValue,slidesObj:member.slides})
  	}

  	return (

		<View style={[
				utilityStyles.column,
				utilityStyles.alignCenter,
				utilityStyles.justifyCenter
			]}>
			<TextInput
				placeholder="Enter a number"
				keyboardType="number-pad"
				placeholderTextColor={options.brandText}
				onChangeText={(val) => changeInputValue(val) }
				style={[
					formStyles.input,
					utilityStyles.fullWidth
				]}
			/>
				<Spacer size={20}/>
				<Button block text="Save" onPress={()=> updateArray('save')}/>
				<Spacer size={20}/>
				<Button block text="Randomise" onPress={()=> updateArray('random')}/>
				<Spacer size={40}/>
		</View>

	);
}
const mapStateToProps = state => ({
	member: state.member || {},
});

const mapDispatchToProps = dispatch => ({
	shuffleSlideArray: dispatch.member.shuffleSlideArray,
});

export default connect(mapStateToProps, mapDispatchToProps)(Form);
