import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

const Spacer = ({ size ,bkg }) => (
  <View style={{ width:'100%' , height: size , backgroundColor: bkg ? bkg : 'transparent' }} />
);

Spacer.propTypes = {
  size: PropTypes.number,
};

Spacer.defaultProps = {
  size: 20,
};

export default Spacer;
