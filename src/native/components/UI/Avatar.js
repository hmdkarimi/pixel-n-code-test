import React from 'react';
import PropTypes from 'prop-types';
import { View , TouchableOpacity ,Image } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
///import styles & variables
import utilityStyles from "../../styles/Utitlity";
import commonStyles from '../../styles/Common';
import options from "../../vars/variable";
/*****BUTTON COMPOENET
 * Availabel config:
 * Avatar => {
 * circle: circle avatar style
   radius: add custom radius to image
   width: avatar width
   height: avatar height
   source : its array of images , if length > 1 it will be slider automatically
 * }
 */
const Avatar = (props) => {
   const { width,height,source,style,radius,circle } = props;
    let imgRadius = radius ? radius : options.borderRadiusBase;
   circle ? imgRadius = width / 2 : imgRadius = imgRadius

   return(
		<Carousel

			inactiveSlideScale={1}
			inactiveSlideOpacity={0}
			autoplay={true}
			autoplayInterval={5000}
			loop={true}
			autoplayDelay={0}
			shouldOptimizeUpdates={false}
			data={source}
			renderItem={({item})=>{
				return(
					<Image  source={{uri:item}}
					 width={width}
					height={height}
					style={[
						commonStyles.avatar,
						{width:width,height:height},
						style,
						{borderRadius: radius ? radius : imgRadius},
						]}
					/>
				);
			}}
			sliderWidth={100}
			itemWidth={100}
			contentContainerCustomStyle={[
			utilityStyles.alignEnd,
			utilityStyles.alignSelfCenter
			]}
			containerCustomStyle={[utilityStyles.alignSelfCenter]}
		/>

   );
};

Avatar.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
};

Avatar.defaultProps = {
  width: 60,
  height: 60,
};

export default Avatar;
