import React from 'react'
import {View,Text,Image} from 'react-native';

///import styles & variables
import utilityStyles from "../../../styles/Utitlity";


const Slide = (props) => {

  const {title , img , description} = props.data;


	return (
	  <View style={[
		utilityStyles.column,
		utilityStyles.justifyCenter,
		utilityStyles.alignCenter,
		utilityStyles.alignSelfCenter,
		utilityStyles.mediumMarginBottom,
		utilityStyles.mediumPaddingHorizontal
	  ]}>
		  {
			img && img !== ""
			 ?<Image style={[ utilityStyles.mediumMarginBottom]} source={img}/>
			 :null
		  }

		  {
			title && title !== ""
			? <Text style={[
			  utilityStyles.darkColor,
			  utilityStyles.largeFontSize,
			  utilityStyles.mediumMarginBottom
			]}>{title}</Text>
			:null

		  }

		  {
			description && description !== ""
			? <Text style={[
			  utilityStyles.grayColor,
			  utilityStyles.baseFontSize,
			  utilityStyles.textAlignCenter
			]}>{description}</Text>
			:null

		  }

	  </View>

	);
}

export default Slide;
