import React , {useState,useRef} from 'react';
import {View,Text,TextInput,TouchableOpacity} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import {Actions} from 'react-native-router-flux';
///import styles & variables
import utilityStyles from "../../styles/Utitlity";
import commonStyles from '../../styles/Common';
import options from "../../vars/variable";

/// custom components
import Button from "../UI/Button";
import Spacer from "../UI/Spacer";
import utilityStyle from '../../styles/Utitlity';



const Intro = (props) => {
  const {slides,_renderItem,goToWelcome,member} = props;
  const [activeItem , setActiveItem] = useState(0);
  const _slider = useRef(null);
  const changeSlide = (index = 1) => {
	setActiveItem(index)
  }
  return (
	  <View style={[utilityStyles.column,utilityStyles.displayFlexFull,utilityStyles.lightBkg,utilityStyles.justifyCenter,utilityStyles.alignCenter,utilityStyles.fullHeight,utilityStyles.fullWidth,utilityStyle.relative]}>

		  <View style={[
				utilityStyles.mediumPaddingHorizontal,
				utilityStyles.lightBkg,
				utilityStyles.fullWidth,

			]}>
			  <View>
				  <Carousel
				  ref={_slider}
				  inactiveSlideScale={1}
				  inactiveSlideOpacity={0}
				  data={slides}
				  renderItem={_renderItem }
				  sliderWidth={options.deviceWidth}
				  itemWidth={options.deviceWidth}
				  contentContainerCustomStyle={[
					utilityStyles.alignEnd,
					utilityStyles.alignSelfCenter
				  ]}
				  containerCustomStyle={[utilityStyles.alignSelfCenter]}
				  onSnapToItem={(index) => changeSlide(index) }
				/>
				<Spacer size={20}/>
				<Pagination
				  carouselRef={_slider}
				  dotsLength={slides.length}
				  activeDotIndex={activeItem}
				  dotColor={options.brandPrimary}
				  inactiveDotColor={options.brandText}
				  inactiveDotOpacity={1}
				  inactiveDotScale={1}
				  tappableDots={!!_slider}
				/>
				</View>
				<Spacer size={45}/>
				  <Button  onPress={() => goToWelcome()}
							hasIcon
							iconSize={22}
							iconName="ios-arrow-round-forward"
							text="Go to Welcome Screen"
				/>
		  </View>
	  </View>

	);
}

export default Intro;
