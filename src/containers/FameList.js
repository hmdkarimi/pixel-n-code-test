import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class FameList extends Component {
  static propTypes = {
	Layout: PropTypes.func.isRequired,

  }

  state = {
	error: null,
	success: null,
	loading: true,
  }

  componentDidMount =() =>{

	this.fetch();

  }
  fetch = (page=0) => {
	const{getFames} = this.props;
	return getFames(page)
	.then((res) => {
		//if fetched list successfully
		console.log(this.props.member)
		this.setState({
			loading: false
		})
	}).catch((err) => {

		throw err; // To prevent transition back
	});
  }

  render = () => {
	const { member, Layout } = this.props;
	const {loading } = this.state;

	return (
	  <Layout
		member={member}
		loading={loading}
		fetch={this.fetch}
	  />
	);
  }
}

const mapStateToProps = state => ({
	member: state.member || {}
});

const mapDispatchToProps = dispatch => ({
	getFames: dispatch.member.getFames
});

export default connect(mapStateToProps, mapDispatchToProps)(FameList);
