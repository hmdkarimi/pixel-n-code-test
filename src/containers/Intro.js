import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import logo from '../images/logo.png';
import Slide from "../native/components/Intro/IntroSlide/Slide";
import { Actions } from 'react-native-router-flux';
class Intro extends Component {
  static propTypes = {
	Layout: PropTypes.func.isRequired,

  }

  state = {
	error: null,
	success: null,
	loading: false,
	slides: [
	  {
		img: logo,
		title:'',
		description:''
	  },
	  {
		img: '',
		title: 'Slide Title',
		description: ' lorem ispum sampletext for slide item sample text'
	  },
	  {
		title: 'Slide Title',
		description: ' lorem ispum sampletext for slide item sample text'
	  }
	]
  }

  componentDidMount = () => {
	const { member} = this.props;
	if(!member.isFirstTime){
		//go to welcome screen
		this.goToWelcome()
	}

  }

  componentDidUpdate = () => {

  }


  _renderSlideItem = ({item ={}, index}) => {
	   //Render intro slider slide items
		return <Slide key={index} data={item} />;
  }


  goToWelcome = () => {

	//Action on tap "go to welcom screen"

	const {firstOpenAction} = this.props;
	return firstOpenAction()
	.then((res) => {
		//if action did successfully
		Actions.welcome();
	}).catch((err) => {

		throw err; // To prevent transition back
	});
}


  render = () => {
	const { member, Layout } = this.props;
	const {loading } = this.state;
	return (
	  <Layout
		member={member}
		loading={!member.isFirstTime}
		goToWelcome = {this.goToWelcome}
		slides={this.state.slides}
		_renderItem={this._renderSlideItem}
	  />
	);

  }
}

const mapStateToProps = state => ({
	member: state.member || {},
});

const mapDispatchToProps = dispatch => ({
	firstOpenAction: dispatch.member.firstOpenAction,
});

export default connect(mapStateToProps, mapDispatchToProps)(Intro);
