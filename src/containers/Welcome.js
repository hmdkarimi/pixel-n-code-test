import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Slide from "../native/components/Intro/IntroSlide/Slide";

class Welcome extends Component {
  static propTypes = {
	Layout: PropTypes.func.isRequired,

  }

  state = {
	error: null,
	success: null,
	loading: false,
  }
 _renderSlideItem = ({item ={}, index}) => {
	   //Render intro slider slide items
		return <Slide key={index} data={item} />;
  }


  render = () => {
	const { member, Layout , loading } = this.props;
	//const {loading } = this.state;
	return (
	  <Layout
		loading={loading}
		member={member}
		_renderItem={this._renderSlideItem}
	  />
	);
  }
}

const mapStateToProps = state => ({
	member: state.member || {},
});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
