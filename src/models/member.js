import { errorMessages } from '../constants/messages';
import axios from 'axios';
import shuffleSeed from 'shuffle-seed';
axios.defaults.baseURL = 'https://halloffame-server.herokuapp.com';

///IMPORT SLIDER IMAGES
import slide1 from '../images/1.gif';
import slide2 from '../images/2.gif';
import slide3 from '../images/3.gif';
import slide4 from '../images/4.gif';
import slide5 from '../images/5.gif';

///generate random number
const generateRandom = (min, max, exceptNum) => {

	let num = Math.floor(Math.random() * (max - min + 1)) + min;
	return (num === exceptNum) ? generateRandom(min, max) : num;

}

const  arrangeArray = (arr, old_index, new_index) => {
	while (old_index < 0) {
		old_index += arr.length;
	}
	while (new_index < 0) {
		new_index += arr.length;
	}
	if (new_index >= arr.length) {
		var k = new_index - arr.length;
		while ((k--) + 1) {
			arr.push(undefined);
		}
	}
	arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
	return arr;
}
//console.log(arrangeArray([10, 20, 30, 40, 50], -1, -2));
export default {
  state: {
	  isFirstTime:true,
	  slides: [{
	  		img: slide1,
	  		title: 'slide 1'
	  	},
	  	{
	  		img: slide2,
	  		title: 'slide 2'
	  	},
	  	{
	  		img: slide3,
	  		title: 'slide 3'
	  	},
	  	{
	  		img: slide4,
	  		title: 'slide 4'
	  	},
	  	{
	  		img: slide5,
	  		title: 'slide 5'
	  	},
	  ],
	  lis:[]
  }, // initial state

  /**
   * Reducers
   */
  reducers: {
    loading(state, loader) {
        return {
          ...state,
          loading: loader
        };
	  },

	  firstOpen(state, status) {
	  	return {
	  		...state,
	  		isFirstTime: status
	  	};
	  },

	  slides(state, items) {
	  	return {
	  		...state,
	  		slides: items
	  	};
	  },

	  list(state, items) {

		let orderList = [];
		//filter to find target actor index
		items.list.filter((item, index) => {
			if (item.name === 'Gwendoline Christie') {
				//change order of item to 3
				orderList = arrangeArray(items.list, index, 2);
			} else {
				///if not current target  exist save list with default order
				orderList = items.list;
			}


		})
		return {
			...state,
			list: orderList
		};

	  },

      error(state, error) {
        return {
          ...state,
          error: error
        };
      },

      success(state, success) {
        return {
          ...state,
          success: success
        };
      },
  },

  /**
   * Effects/Actions
   */
  effects: dispatch => ({
    /**
     * Sign Up
     *
     * @param {obj} formData - data from form
     * @return {Promise}
     */
    // RECIPE // POST
    getFames(current) {

      this.loading(true);
      return new Promise(async (resolve, reject) => {
        // if (!name) return reject({ message: errorMessages.missingEmail });
        axios({
            method: 'get',
            url: '/fames',
			responseType: 'json',
			params:{
				guest:true,
				page: 0
			}
          })
          .then(response => {
            if (!response) {
               reject({
                message: response
              });
            } else {
			  this.success('List Fetched');
			  this.list({list:response.data.data.list,page:current});
			  resolve({list:response.data.data.list,page:current});
            }
          })
          .catch(function (error) {
            debugger
             reject(error);
          });
      }).catch(err => {
        //throw err.message;
         reject();
      });
    },

    firstOpenAction(data) {

      this.loading(true);
      return new Promise(async (resolve, reject) => {
		this.firstOpen(false) // Send to reducer
		resolve(false);
      }).catch(err => {

         reject({
          message: err
        });

      });
	},

	shuffleSlideArray(obj) {

		this.loading(true);
		return new Promise(async (resolve, reject) => {
			let newSlidesArray = [];
			if (obj.type === 'save') {

				//shuffle the slides array
				newSlidesArray = shuffleSeed.shuffle(obj.slidesObj, obj.value);
				this.slides(newSlidesArray) // Send to reducer

			} else {

				const randomNumber = generateRandom(1, 9, obj.value);
				newSlidesArray = shuffleSeed.shuffle(obj.slidesObj, randomNumber);
				this.slides(newSlidesArray) // Send to reducer

			}
			this.loading(false);
			resolve();

		}).catch(err => {

			reject({
				message: err
			});

		});
	},


  }),
};
